package first_task;

import java.io.*;
import java.util.Random;
import java.util.Scanner;

/**
 * Первая подзадача проекта ManyFiles
 * Предназначение - создание файла целых чисел
 * Числа создаются рандомно посредством использования класcа Random
 *
 * Используемые методы:
 * - main
 * - fillingMasive(заполнение файла рандомно сгенерированными целыми числами)
 *
 * @author Isheykin A.A.
 */
public class FirstTaskMain {
    static Scanner scanner = new Scanner(System.in);
    static Random random = new Random();
    public static void main(String[] args) throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("intdata.dat"));
        System.out.println("Введите необходимое количество чисел в файле - ");
        int countOfNumbers = scanner.nextInt();
        fillingFile(countOfNumbers, dataOutputStream);
        dataOutputStream.close();
    }

    /**
     * Задачи метода:
     * - генерация случайного числа в диапазоне (0;9999999)
     * - запись сгенерированных чисел в файл "intdata.dat"
     * @param countOfNumbers
     * @param dataOutputStream
     * @throws IOException
     *
     * @author Isheykin A.A.
     */
    private static void fillingFile(int countOfNumbers, DataOutputStream dataOutputStream) throws IOException {
        for (int counter = 1; counter <= countOfNumbers; counter++) {
            int randomNum = random.nextInt(9999999);
            System.out.println(randomNum);
            dataOutputStream.writeInt(randomNum);
        }
    }
}
