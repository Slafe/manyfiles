package second_task;

import java.io.*;

/**
 * Третья подзадача проекта ManyFiles
 * Предназначение - на основе файла "int6data.dat" сформировать текстовый файл "txt6data.dat", содержащий только счастливые номера
 * (сумма левых 3-х цифр равна сумме правых 3-х цифр)
 *
 * Используемые методы :
 * - main
 *
 * @author Isheykin A.A.
 */
public class SecondTaskMain {
    /**
     * Метод Main
     * Предназначение - формирование файла, содержащего только счастливые числа
     * @param args
     * @throws IOException
     *
     * @author Isheykin A.A.
     */
    public static void main(String[] args) throws IOException {

        DataInputStream input = new DataInputStream(new FileInputStream("intdata.dat"));
        DataOutputStream output = new DataOutputStream(new FileOutputStream("int6data.dat"));

        Integer currentNumber;

        while (input.available() != 0) {
            try {
                currentNumber = input.readInt();
                if (currentNumber.toString().length() <= 6) {
                    output.writeInt(currentNumber);
                }
            } catch (java.io.EOFException END_FILE_EXCEPTION) {
                break;
            }
        }
        input.close();
        output.close();
    }
}
