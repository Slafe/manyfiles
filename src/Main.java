import first_task.FirstTaskMain;
import second_task.SecondTaskMain;
import third_task.ThirdTaskMain;

/**
 * Main-класс, предназначенный для вызова каждого main-класса 3-х задач
 */
public class Main {
    // Здесь будут поочередно вызываться каждый main-класс
}
