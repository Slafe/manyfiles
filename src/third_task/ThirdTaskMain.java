package third_task;

import java.io.*;
import java.util.ArrayList;

/**
 * Вторая подзадача проекта ManyFiles
 * Предназначение - на основе данных файла "intdata.dat" формирование файла, содержащего только шестизначное целые числа "int6data.dat"
 *
 * Исользуемые методы6
 * - searchLuckyNumbers
 * - filingMassive
 * - isNumberLucky
 *
 * @author Isheykin A.A.
 */
public class ThirdTaskMain {
    public static void main(String[] args) throws IOException {
        DataInputStream inputStream = new DataInputStream(new FileInputStream("int6data.dat"));
        BufferedWriter writer = new BufferedWriter(new FileWriter("result.txt"));
        ArrayList<String> luckyNumbers = searchLuckyNumbers(inputStream);
    }

    /**
     * Метод searchLuckyNumbers
     * Предназначение - поиск счастливых чисел и их последующая запись в ArrayList
     *
     * @param inputStream
     * @return
     * @throws IOException
     *
     * @author Isheykin A.A.
     */
    private static ArrayList<String> searchLuckyNumbers(DataInputStream inputStream) throws IOException {
        ArrayList<String> luckyNumbers = new ArrayList<>();
        int[] currentNumber_massive;
        int currentNumber;
        String currentNumber_string;
        boolean isLucky;
        while (inputStream.available() != 0) {
            try {
                currentNumber = inputStream.readInt();
                currentNumber_massive = fillingMassive(currentNumber);
                currentNumber_string = String.valueOf(currentNumber);
                isLucky = isNumberLucky(currentNumber_massive);
                if (isLucky) {
                    luckyNumbers.add(currentNumber_string);
                }
            } catch (IOException endedFileException) {
                return luckyNumbers;
            }
        }
        return luckyNumbers;
    }

    /**
     * Метод fillingMassive
     * Предназначение - представление 6-ного числа в виде массива
     * @param currentNumber
     * @return
     */
    private static int[] fillingMassive(int currentNumber) {
        int[] massive = new int[6];
        String number = String.valueOf(currentNumber);
        for (int numberSymbol = 5; numberSymbol >= 0; numberSymbol--) {
            try {
                massive[numberSymbol] = number.charAt(numberSymbol);
            } catch (StringIndexOutOfBoundsException indexOutFromLine) {
                massive[numberSymbol] = 0;
            }
        }
        System.out.println("fillingMassive. Массив заполнен. " + massive.toString());
        return massive;
    }

    /**
     * Метод isNumberLucky
     * Предназначение - определение, является ли переданное число в представлении 6-ого массива счастливым числом
     * @param currentNumber
     * @return boolean значение о "счастливости" числа
     */
    private static boolean isNumberLucky(int[] currentNumber) {
        int leftAmount = currentNumber[0] + currentNumber[1] + currentNumber[2];
        int rightAmount = currentNumber[3] + currentNumber[4] + currentNumber[5];
        if (leftAmount==rightAmount) {
            return true;
        } else {
            return false;
        }
    }
}
